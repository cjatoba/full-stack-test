<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    public function __construct(private User $repository)
    {
    }

    public function getAll($withPaginate = false)
    {
        return $withPaginate ? $this->repository->paginate(10) : $this->repository->all();
    }

    public function getById(string $uuid)
    {
        return $this->repository->findOrFail($uuid);
    }

    public function update(string $uuid, array $userData)
    {
        $user = $this->getById($uuid);

        return $user->update($userData);
    }

    public function destroy(string $uuid)
    {
        $user = $this->getById($uuid);

        return $user->delete();
    }
}
