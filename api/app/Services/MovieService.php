<?php

namespace App\Services;

use App\Models\Movie;

class MovieService
{
    public function __construct(private Movie $repository)
    {
    }

    public function getAll($withPaginate = false)
    {
        return $withPaginate ? $this->repository->paginate(10) : $this->repository->all();
    }

    public function store(array $movieData)
    {
        return $this->repository->create($movieData);
    }

    public function getById(string $uuid)
    {
        return $this->repository->findOrFail($uuid);
    }

    public function update(string $uuid, array $movieData)
    {
        $movie = $this->getById($uuid);

        return $movie->update($movieData);
    }

    public function destroy(string $uuid)
    {
        $movie = $this->getById($uuid);

        return $movie->delete();
    }
}
