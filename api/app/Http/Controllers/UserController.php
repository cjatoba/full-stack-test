<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    public function __construct(private readonly UserService $userService)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = $this->userService->getAll(withPaginate: true);

        return UserResource::collection($users);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        $user = $this->userService->getById($user->id);

        return UserResource::make($user);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $userData = $request->validated();
        $this->userService->update($user->id, $userData);

        return response()->json([
            'data' => [
                'message' => 'Usuário atualizado com sucesso!'
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $deleted = $this->userService->destroy($user->id);

        if (!$deleted) abort(500);

        return response()->json([
            'data' => [
                'message' => 'Usuário deletado com sucesso!'
            ]
        ]);
    }
}
