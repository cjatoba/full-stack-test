<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMovieRequest;
use App\Http\Requests\UpdateMovieRequest;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Services\MovieService;

class MovieController extends Controller
{
    public function __construct(private MovieService $movieService)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $movies = $this->movieService->getAll(withPaginate: true);

        return MovieResource::collection($movies);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMovieRequest $request)
    {
        $movieData = $request->validated();

        $movie = $this->movieService->store($movieData);

        return MovieResource::make($movie);
    }

    /**
     * Display the specified resource.
     */
    public function show(Movie $movie)
    {
        $movie = $this->movieService->getById($movie->id);

        return MovieResource::make($movie);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMovieRequest $request, Movie $movie)
    {
        $movieData = $request->validated();
        $this->movieService->update($movie->id, $movieData);

        return response()->json([
            'data' => [
                'message' => 'Filme atualizado com sucesso!'
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Movie $movie)
    {
        $deleted = $this->movieService->destroy($movie->id);

        if (!$deleted) abort(500);

        return response()->json([
            'data' => [
                'message' => 'Filme deletado com sucesso!'
            ]
        ]);
    }
}
