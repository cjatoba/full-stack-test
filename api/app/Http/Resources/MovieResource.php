<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'duration' => $this->duration,
            'image' => $this->image,
            'created_at' => Carbon::make($this->created_at)->format('d/m/Y H:i:s'),
            'updated_at' => Carbon::make($this->updated_at)->format('d/m/Y H:i:s'),
        ];
    }
}
