<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    public function test_get_all_users()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->getJson('/api/users');

        $response->assertStatus(200);
    }

    public function test_get_all_users_without_user_authenticated()
    {
        $response = $this->getJson('/api/users');

        $response->assertStatus(401);
    }

    public function test_get_count_users()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        User::factory()->count(10)->create();

        $response = $this->getJson('/api/users');

        $response->assertJsonCount(10, 'data');

        $response->assertStatus(200);
    }

    public function test_not_found_users()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->getJson('/api/users/fake_value');

        $response->assertStatus(404);
    }

    public function test_get_user()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = User::factory()->create();

        $response = $this->getJson("/api/users/{$user->id}");

        $response->assertStatus(200);
    }

    public function test_get_user_without_user_authenticated()
    {
        $user = User::factory()->create();

        $response = $this->getJson("/api/users/{$user->id}");

        $response->assertStatus(401);
    }

    public function test_validation_update_user()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = User::factory()->create();

        $response = $this->putJson("/api/users/{$user->id}", []);

        $response->assertStatus(422);
    }

    public function test_404_update_user()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->putJson("/api/users/fake_value", [
            'name' => fake()->name,
        ]);

        $response->assertStatus(404);
    }

    public function test_update_user()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = User::factory()->create();

        $response = $this->putJson("/api/users/{$user->id}", [
            'name' => fake()->name,
            'email' => fake()->email,
        ]);

        $response->assertStatus(200);
    }

    public function test_update_user_without_user_authenticated()
    {
        $user = User::factory()->create();

        $response = $this->putJson("/api/users/{$user->id}", [
            'name' => fake()->name,
            'email' => fake()->email,
        ]);

        $response->assertStatus(401);
    }

    public function test_404_delete_user()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->deleteJson("/api/users/fake_value");

        $response->assertStatus(404);
    }

    public function test_delete_user()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = User::factory()->create();

        $response = $this->deleteJson("/api/users/{$user->id}");

        $response->assertStatus(200);
    }

    public function test_delete_user_without_user_authenticated()
    {
        $user = User::factory()->create();

        $response = $this->deleteJson("/api/users/{$user->id}");

        $response->assertStatus(401);
    }
}
