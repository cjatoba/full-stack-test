<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Movie;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class MovieControllerTest extends TestCase
{
    public function test_get_all_movies()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->getJson('/api/movies');

        $response->assertStatus(200);
    }

    public function test_get_all_movies_without_user_authenticated()
    {
        $response = $this->getJson('/api/movies');

        $response->assertStatus(401);
    }

    public function test_get_count_movies()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        Movie::factory()->count(10)->create();

        $response = $this->getJson('/api/movies');

        $response->assertJsonCount(10, 'data');

        $response->assertStatus(200);
    }

    public function test_not_found_movies()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->getJson('/api/movies/fake_value');

        $response->assertStatus(404);
    }

    public function test_get_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = Movie::factory()->create();

        $response = $this->getJson("/api/movies/{$user->id}");

        $response->assertStatus(200);
    }

    public function test_validations_create_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->postJson('/api/movies', []);

        $response->assertStatus(422);
    }

    public function test_create_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->postJson('/api/movies', [
            'name' => fake()->name(),
            'description' => fake()->text,
            'duration' => fake()->time,
            'image' => fake()->url
        ]);

        $response->assertStatus(201);
    }

    public function test_get_movie_without_user_authenticated()
    {
        $user = Movie::factory()->create();

        $response = $this->getJson("/api/movies/{$user->id}");

        $response->assertStatus(401);
    }

    public function test_validation_update_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = Movie::factory()->create();

        $response = $this->putJson("/api/movies/{$user->id}", []);

        $response->assertStatus(422);
    }

    public function test_404_update_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->putJson("/api/movies/fake_value", [
            'name' => fake()->name,
        ]);

        $response->assertStatus(404);
    }

    public function test_update_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = Movie::factory()->create();

        $response = $this->putJson("/api/movies/{$user->id}", [
            'name' => fake()->name(),
            'description' => fake()->text,
            'duration' => fake()->time,
            'image' => fake()->url
        ]);

        $response->assertStatus(200);
    }

    public function test_update_movie_without_user_authenticated()
    {
        $user = Movie::factory()->create();

        $response = $this->putJson("/api/movies/{$user->id}", [
            'name' => fake()->name,
            'email' => fake()->email,
        ]);

        $response->assertStatus(401);
    }

    public function test_404_delete_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $response = $this->deleteJson("/api/movies/fake_value");

        $response->assertStatus(404);
    }

    public function test_delete_movie()
    {
        Sanctum::actingAs(
            User::factory()->create()
        );

        $user = Movie::factory()->create();

        $response = $this->deleteJson("/api/movies/{$user->id}");

        $response->assertStatus(200);
    }

    public function test_delete_movie_without_user_authenticated()
    {
        $user = Movie::factory()->create();

        $response = $this->deleteJson("/api/movies/{$user->id}");

        $response->assertStatus(401);
    }
}
