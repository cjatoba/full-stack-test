import Label from '@/components/Label'
import Input from '@/components/Input'
import InputError from '@/components/InputError'
import Button from '@/components/Button'
import { useState } from 'react'
import axios from '@/lib/axios'
import Toast from '@/lib/toast'

const FormEditUser = ({user}) => {
    const [name, setName] = useState(user.name);
    const [email, setEmail] = useState(user.email);
    const [errors, setErrors] = useState([]);

    const submitForm = async event => {
        event.preventDefault();

        setErrors([]);

        try {
            const response = await axios.put(
                `/api/users/${user.id}`,
                {
                    name: event.target.name.value,
                    email: event.target.email.value
                }
            );
            if (response.status === 200) {
                Toast.fire({
                    icon: 'success',
                    title: 'Usuário atualizado com sucesso'
                })
            }
        } catch (error) {
            Toast.fire({
                icon: 'error',
                title: 'Falha ao atualizar o usuário'
            })
            if (error?.response?.data?.errors) {
                setErrors(error.response.data.errors)
            }
        }

    }

    return (
        <form onSubmit={submitForm}>
            <div>
                <Label htmlFor="name">Nome</Label>

                <Input
                    id="name"
                    type="text"
                    value={name}
                    className="block mt-1 w-full"
                    onChange={event => setName(event.target.value)}
                    required
                    autoFocus
                />

                <InputError messages={errors.name} className="mt-2" />
            </div>

            <div className="mt-4">
                <Label htmlFor="email">Email</Label>

                <Input
                    id="email"
                    type="email"
                    value={email}
                    className="block mt-1 w-full"
                    onChange={event => setEmail(event.target.value)}
                    required
                />

                <InputError messages={errors.email} className="mt-2" />
            </div>

            <div className="mt-4">
                <Button>Atualizar</Button>
            </div>
        </form>
    )
}

export default FormEditUser
