import AppLayout from '@/components/Layouts/AppLayout'
import Head from 'next/head'
import Card from '@/components/Card'
import FormRegisterMovie from '@/components/Movies/FormRegisterMovie'

const MoviesLayout = ({children}) => {
    return (
        <AppLayout
            header={
                <h2 className='font-semibold text-xl text-gray-800 leading-tight uppercase'>
                    Filmes
                </h2>
            }>

            <Head>
                <title>Watch - Filmes</title>
            </Head>

            <main>
                {children}
            </main>

        </AppLayout>
    )
}

export default MoviesLayout
