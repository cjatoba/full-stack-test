import AppLayout from '@/components/Layouts/AppLayout'
import Head from 'next/head'
import Card from '@/components/Card'
import FormRegisterMovie from '@/components/Movies/FormRegisterMovie'
import CardLink from '@/components/CardLink'

const UsersLayout = ({children}) => {
    return (
        <AppLayout
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight uppercase">
                    Usuários
                </h2>
            }>

            <Head>
                <title>Watch - Usuários</title>
            </Head>

            <main>
                {children}
            </main>
        </AppLayout>
    )
}

export default UsersLayout
