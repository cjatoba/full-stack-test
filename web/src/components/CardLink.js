import Card from '@/components/Card'
import Link from 'next/link'

const CardLink = ({children, ...props}) => {
    return (
        <Link {...props}>
            <Card className="bg-gray-800 text-center uppercase text-lg text-white">
                {children}
            </Card>
        </Link>
    )
}

export default CardLink
