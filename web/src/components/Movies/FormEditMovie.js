import Label from '@/components/Label'
import Input from '@/components/Input'
import InputError from '@/components/InputError'
import Button from '@/components/Button'
import { useState } from 'react'
import axios from '@/lib/axios'
import Toast from '@/lib/toast'

const FormEditMovie = ({ movie }) => {
    const [name, setName] = useState(movie.name);
    const [description, setDescription] = useState(movie.description);
    const [duration, setDuration] = useState(movie.duration);
    const [image, setImage] = useState(movie.image);
    const [errors, setErrors] = useState([]);

    const submitForm = async event => {
        event.preventDefault();

        setErrors([]);

        try {
            const response = await axios.put(
                `/api/movies/${movie.id}`,
                {
                    name: event.target.name.value,
                    description: event.target.description.value,
                    duration: event.target.duration.value,
                    image: event.target.image.value
                }
            );
            if (response.status === 200) {
                Toast.fire({
                    icon: 'success',
                    title: 'Filme atualizado com sucesso'
                })
            }
        } catch (error) {
            Toast.fire({
                icon: 'error',
                title: 'Falha ao atualizar o filme'
            })
            if (error?.response?.data?.errors) {
                setErrors(error.response.data.errors)
            }
        }
    }

    return (
        <form onSubmit={submitForm}>
            <div>
                <Label htmlFor="name">Nome</Label>

                <Input
                    id="name"
                    type="text"
                    value={name}
                    className="block mt-1 w-full"
                    onChange={event => setName(event.target.value)}
                    required
                    autoFocus
                />

                <InputError messages={errors.name} className="mt-2" />
            </div>

            <div className="mt-4">
                <Label htmlFor="description">Descrição</Label>

                <Input
                    id="description"
                    type="text"
                    value={description}
                    className="block mt-1 w-full"
                    onChange={event => setDescription(event.target.value)}
                    required
                />

                <InputError messages={errors.description} className="mt-2" />
            </div>

            <div className="mt-4">
                <Label htmlFor="duration">Duração</Label>

                <Input
                    id="duration"
                    type="text"
                    value={duration}
                    placeholder="Ex. 02:45:00"
                    className="block mt-1 w-full"
                    onChange={event => setDuration(event.target.value)}
                    required
                />

                <InputError messages={errors.duration} className="mt-2" />
            </div>

            <div className="mt-4">
                <Label htmlFor="image">URL da imagem</Label>

                <Input
                    id="image"
                    type="url"
                    value={image}
                    className="block mt-1 w-full"
                    onChange={event => setImage(event.target.value)}
                    required
                />

                <InputError messages={errors.image} className="mt-2" />
            </div>

            <div className="mt-4">
                <Button>Atualizar</Button>
            </div>
        </form>
    )
}

export default FormEditMovie
