import FormRegisterMovie from '@/components/Movies/FormRegisterMovie'
import Card from '@/components/Card'
import MoviesLayout from '@/components/Layouts/MoviesLayout'

const Create = () => {
    return (
        <MoviesLayout>
            <Card>
                <FormRegisterMovie />
            </Card>
        </MoviesLayout>
    )
}

export default Create
