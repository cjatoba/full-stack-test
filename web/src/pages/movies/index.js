import CardLink from '@/components/CardLink'
import MoviesLayout from '@/components/Layouts/MoviesLayout'

const Index = () => {
    return (
        <MoviesLayout>
            <CardLink href="/movies/create">Cadastrar um novo filme</CardLink>
            <CardLink href="/movies/list">Listar todos os filmes</CardLink>
        </MoviesLayout>
    )
}

export default Index
