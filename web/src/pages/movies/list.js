import Card from '@/components/Card'
import axios from '@/lib/axios'
import Link from 'next/link'
import { useState } from 'react'
import Toast from '@/lib/toast'
import Swal from 'sweetalert2'
import MoviesLayout from '@/components/Layouts/MoviesLayout'
import Image from 'next/image'

const getAllMovies = async (context = null) => {
    const headers = context ? {
        headers: {
            origin: 'localhost',
            Cookie: context.req.headers.cookie,
        },
    } : null;

    const response = await axios.get(
        '/api/movies',
        headers
    );

    return response.data;
}

export async function getServerSideProps(context) {
    const allMoviesData = await getAllMovies(context);

    return {
        props: {
            allMoviesData,
        }
    }
}

const List = ({ allMoviesData }) => {
    const [movies, setMovies] = useState(allMoviesData)
    const handleMovieDelete = async (event) => {
        const movieId = event.target.dataset.movieId

        Swal.fire({
            title: 'Tem certeza que deseja deletar este filme?',
            text: "Você não será capaz de reverter isso!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, deletar!',
            focusCancel: true,
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const response = await axios.delete(`api/movies/${movieId}`)

                    if (response.status === 200) {
                        Toast.fire({
                            icon: 'success',
                            title: 'Filme deleteado com sucesso!'
                        })
                        setMovies(await getAllMovies());
                    }

                } catch (error) {
                    console.log(error)
                    Toast.fire({
                        icon: 'error',
                        title: 'Falha ao deletar o filme!'
                    })
                }
            }
        })
    }
    return (
        <MoviesLayout>
            <Card>
                <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead
                            className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                Imagem
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Nome
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Descrição
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Duração
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Data de criação
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Data de atualização
                            </th>
                            <th scope="col" className="px-6 py-3 uppercase text-center">
                                <span>Ações</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            {movies.data.map((movie) => (
                                <tr key={movie.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                    <td className="px-6 py-4">
                                        <img
                                            src={movie.image} alt={movie.description}
                                            width="50"
                                            height="50"
                                        />
                                    </td>
                                    <th scope="row"
                                        className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {movie.name}
                                    </th>
                                    <td className="px-6 py-4">
                                        {movie.description}
                                    </td>
                                    <td className="px-6 py-4">
                                        {movie.duration}
                                    </td>
                                    <td className="px-6 py-4">
                                        {movie.created_at}
                                    </td>
                                    <td className="px-6 py-4">
                                        {movie.updated_at}
                                    </td>
                                    <td className="px-6 py-4 text-center">
                                        <Link
                                            href={`/movies/edit/${movie.id}`}
                                            className="font-medium text-blue-600 dark:text-blue-500 hover:underline uppercase">
                                            Editar
                                        </Link>
                                        <a
                                            onClick={handleMovieDelete}
                                            data-movie-id={movie.id}
                                            className="font-medium text-red-600 dark:text-red-500 hover:underline uppercase ml-2 cursor-pointer">
                                            Deletar
                                        </a>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </Card>
        </MoviesLayout>
    )
}

export default List
