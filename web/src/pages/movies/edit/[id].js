import Card from '@/components/Card'
import axios from '@/lib/axios'
import MoviesLayout from '@/components/Layouts/MoviesLayout'
import FormEditMovie from '@/components/Movies/FormEditMovie'

export async function getServerSideProps(context) {
    const { params } = context;
    const headers = {
        headers: {
            origin: 'localhost',
            Cookie: context.req.headers.cookie,
        },
    };

    const response = await axios.get(`/api/movies/${params.id}`, headers)
    const movieData = response.data

    return {
        props: {
            movie: movieData.data,
        },
    }
}

const Movie = ({ movie }) => {
    return (
        <MoviesLayout>
            <Card>
                <FormEditMovie movie={movie} />
            </Card>
        </MoviesLayout>
    )
}

export default Movie
