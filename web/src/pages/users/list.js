import UsersLayout from '@/components/Layouts/UsersLayout'
import Card from '@/components/Card'
import axios from '@/lib/axios'
import Link from 'next/link'
import { useState } from 'react'
import Toast from '@/lib/toast'
import Swal from 'sweetalert2'

const getAllUsers = async (context = null) => {
    const headers = context ? {
        headers: {
            origin: 'localhost',
            Cookie: context.req.headers.cookie,
        },
    } : null;
    const response = await axios.get('/api/users', headers);
    return response.data;
}
export async function getServerSideProps(context) {
    const allUsersData = await getAllUsers(context);

    return {
        props: {
            allUsersData,
        }
    }
}

const List = ({allUsersData}) => {
    const [users, setUsers] = useState(allUsersData)
    const handleUserDelete = async (event) => {
        const userId = event.target.dataset.userId

        Swal.fire({
            title: 'Tem certeza que deseja deletar este usuário?',
            text: "Você não será capaz de reverter isso!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, deletar!',
            cancelButtonText: 'Cancelar',
            focusCancel: true,
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const response = await axios.delete(`api/users/${userId}`)

                    if (response.status === 200) {
                        Toast.fire({
                            icon: 'success',
                            title: 'Usuário deleteado com sucesso!'
                        })
                        setUsers(await getAllUsers());
                    }

                } catch (error) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Falha ao deletar o usuário!'
                    })
                }
            }
        })
    }
    return (
        <UsersLayout>
            <Card>
                <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead
                            className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                Nome
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Email
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Data de criação
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Data de atualização
                            </th>
                            <th scope="col" className="px-6 py-3 uppercase text-center">
                                <span>Ações</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            {users.data.map((user) => (
                                <tr key={user.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                    <th scope="row"
                                        className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {user.name}
                                    </th>
                                    <td className="px-6 py-4">
                                        {user.email}
                                    </td>
                                    <td className="px-6 py-4">
                                        {user.created_at}
                                    </td>
                                    <td className="px-6 py-4">
                                        {user.updated_at}
                                    </td>
                                    <td className="px-6 py-4 text-center">
                                        <Link
                                            href={`/users/edit/${user.id}`}
                                            className="font-medium text-blue-600 dark:text-blue-500 hover:underline uppercase">
                                            Editar
                                        </Link>
                                        <a
                                            onClick={handleUserDelete}
                                            data-user-id={user.id}
                                            className="font-medium text-red-600 dark:text-red-500 hover:underline uppercase ml-2 cursor-pointer">
                                            Deletar
                                        </a>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </Card>
        </UsersLayout>
    )
}

export default List
