import Card from '@/components/Card'
import UsersLayout from '@/components/Layouts/UsersLayout'
import FormEditUser from '@/components/Users/FormEditUser'
import { useRouter } from 'next/router'
import axios from '@/lib/axios'

export async function getServerSideProps(context) {
    const headers = {
        headers: {
            origin: 'localhost',
            Cookie: context.req.headers.cookie,
        },
    };
    const { params } = context;
    const response = await axios.get(`/api/users/${params.id}`, headers)
    const userData = response.data

    return {
        props: {
            user: userData.data,
        },
    }
}

const User = ({ user }) => {
    return (
        <UsersLayout>
            <Card>
                <FormEditUser user={user} />
            </Card>
        </UsersLayout>
    )
}

export default User
