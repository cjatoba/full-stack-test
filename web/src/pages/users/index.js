import CardLink from '@/components/CardLink'
import UsersLayout from '@/components/Layouts/UsersLayout'

const Index = () => {
    return (
        <UsersLayout>
            <CardLink href="/users/list">Listar todos os usuários</CardLink>
        </UsersLayout>
    )
}

export default Index
