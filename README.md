## Teste Desenvolvedor Full Stack

* O Projeto foi desenvolvido utilizando docker com o ambiente do Laravel Sail 

## Passos para execução da aplicação:

### Backend

- Clonar o projeto:
```shell
git clone git@gitlab.com:cjatoba/full-stack-test.git
```

- Acessar o diretório do projeto backend:
```shell
cd full-stack-test/api
```

- Copiar o arquivo `.env.example` para `.env`:
```shell
cp .env.example .env
```

- *A chave APP_URL deve estar de acordo com o domínio e porta que a aplicação backend será executada;
- *A chave FRONTEND_URL deve estar de acordo com o domínio e porta que a aplicação frontend será executada;

- Instalar as dependência do composer utilizando a imagem laravelsail:
```bash
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```

- Subir os containers docker com o Laravel Sail:
```shell
./vendor/bin/sail up -d
```

- Gerar a APP_KEY do projeto:
```shell
./vendor/bin/sail art key:generate
```

- Rodar as migrations para criação das tabelas:
```shell
./vendor/bin/sail art migrate
```

- Acessar o endereço `http://localhost` para conferir se a aplicação backend está ok, deve ser exibido o conteúdo abaixo:
```json
{
  "Laravel": "10.3.3"
}
```

- Para execução dos testes utilizar o comando abaixo:
```shell
./vendor/bin/sail test
```

### Frontend

- Acessar o diretório do projeto frontend:
```shell
cd ../web
```

- Copiar o arquivo `.env.example`para `.env.local`:
```shell
cp .env.example .env.local
```

- * A chave NEXT_PUBLIC_BACKEND_URL= virá com o valor padrão `http://localhost:80` caso a aplicação backend esteja rodando em uma porta diferente da 80 este endereço deve ser atualizado;

- Instalar as dependências Javascript (É recomendável ter pelo menos a versão 18 do Node instalada):
```shell
npm install
```

- Iniciar a aplicação:
```shell
npm run dev
```

- Acessar a aplicação frontend no endereço `http://localhost:3000`;

- *Caso a aplicação suba em uma porta diferente da 3000, é necessário atualizar a chave FRONTEND_URL no .env da aplicação backend

- Na área superior direita da tela clicar na opção `Registrar`;

- Preencher os campos do cadastro e clicar em `Registrar`;
